package com.github.danleyb2.pizaa.activity;

public class ActivityConstants {

    /**
     * Bundle key for passing a connection around by it's name
     **/
    public static final String CONNECTION_KEY = "CONNECTION_KEY";

    public static final String AUTO_CONNECT = "AUTO_CONNECT";
    public static final String CONNECTED = "CONNECTEd";

    public static final String LOGGING_KEY = "LOGGING_ENABLED";

    public static final String ConnectionStatusProperty = "connectionStatus";


    /**
     * Empty String for comparisons
     **/
    static final String empty = new String();
}
