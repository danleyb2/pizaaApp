package com.github.danleyb2.pizaa.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ndiek on 9/4/2016.
 */
public class Person extends RealmObject {
    @PrimaryKey
    private String phone;
    private String name;

    //private RealmList<Dog> dogs; // Declare one-to-many relationships

    @Override
    public String toString() {
        return name;
    }

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
