package com.github.danleyb2.pizaa.config;

/**
 * Created by ndiek on 7/29/2016.
 */
public class Topics {
    public static final String ORDERS = "orders";
    public static final String LOCATIONS = "locations";
    public static final String WILLS = "riders/status";
    public static final String RIDERS = "riders";
}
