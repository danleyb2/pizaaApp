package com.github.danleyb2.pizaa.model;


import com.google.android.gms.maps.model.LatLng;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ndiek on 8/16/2016.
 */
public class Order extends RealmObject {

    private static final long serialVersionUID = 6066777304350339588L;
    @PrimaryKey
    private long id;
    private int quantity;
    private double lat;
    private double lng;
    private Person recipient;

    public Order() {

    }

    public Order(LatLng endpoint) {
        this.setLocation(endpoint);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Person getRecipient() {
        return recipient;
    }

    public void setRecipient(Person recipient) {
        this.recipient = recipient;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /*
            public MqttMessage getPubMessage(String direction) {

                MqttMessage message = null;
                JSONObject newOrderJsonObject;
                try {
                    newOrderJsonObject = MessagesManager.createNewOrderMessage(this);
                    newOrderJsonObject.put(Keys.DIRECTION,direction);
                    message = new MqttMessage(newOrderJsonObject.toString().getBytes());
                    message.setQos(0);

                } catch (JSONException e) {
                    e.printStackTrace();

                }
                return message;
            }
            */
    @Override
    public String toString() {
        return this.getLocation().toString();
    }

    public LatLng getLocation() {
        return new LatLng(lat, lng);
    }

    public void setLocation(LatLng location) {
        this.lat = location.latitude;
        this.lng = location.longitude;
    }
}
