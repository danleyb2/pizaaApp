package com.github.danleyb2.pizaa.core;


import com.github.danleyb2.pizaa.config.Keys;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by ndiek on 8/18/2016.
 */
public class MessagesManager {


    public static JSONObject firstWill(String clientId) throws JSONException {
        JSONObject jsonObjectFirstWill = new JSONObject();
        jsonObjectFirstWill.put(Keys.RIDER, createClientJsonObject(clientId));
        jsonObjectFirstWill.put(Keys.ALIVE, 1);
        return jsonObjectFirstWill;
    }

    private static JSONObject createClientJsonObject(String clientId) throws JSONException {
        JSONObject client = new JSONObject();
        client.put("id", clientId);
        return client;
    }

    public static JSONObject lastWill(String clientId) throws JSONException {

        JSONObject jsonObjectFirstWill = new JSONObject();
        jsonObjectFirstWill.put(Keys.RIDER, createClientJsonObject(clientId));
        jsonObjectFirstWill.put(Keys.ALIVE, 0);
        return jsonObjectFirstWill;
    }


    public static JSONObject locationUpdate(LatLng latLng, String clientId) throws JSONException {
        JSONObject body = new JSONObject();
        JSONObject client = createClientJsonObject(clientId);
        JSONObject location = getLocationJsonObject(latLng);

        body.put(Keys.RIDER, client);
        body.put(Keys.LOCATION, location);

        return body;

    }

    private static JSONObject getLocationJsonObject(LatLng latLng) throws JSONException {
        JSONObject location = new JSONObject();
        location.put(Keys.LATITUDE, latLng.latitude);
        location.put(Keys.LONGITUDE, latLng.longitude);
        location.put(Keys.NAME, "");
        return location;
    }

}
