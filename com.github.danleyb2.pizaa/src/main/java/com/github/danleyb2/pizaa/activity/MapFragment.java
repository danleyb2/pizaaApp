package com.github.danleyb2.pizaa.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Fragment;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.github.danleyb2.pizaa.R;
import com.github.danleyb2.pizaa.core.OrdersManager;
import com.github.danleyb2.pizaa.core.RouteManager;
import com.github.danleyb2.pizaa.model.Order;
import com.github.danleyb2.pizaa.model.PickUpLocation;
import com.github.danleyb2.pizaa.model.Route;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import java.util.ArrayList;

import io.realm.RealmChangeListener;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;


public class MapFragment extends Fragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private static final String TAG = MapFragment.class.getSimpleName();
    SharedPreferences preferences;
    private Marker mCurrLocationMarker;
    private GoogleMap mMap;
    private MapView mapView;
    private GoogleApiClient mGoogleApiClient;
    private FloatingActionButton fab;
    private CoordinatorLayout coordinatorLayout;
    private View bottomSheet;
    private BottomSheetBehavior behavior;
    private ArrayList<Polyline> polylines = new ArrayList<>();
    private ArrayList<Marker> orderMarkers = new ArrayList<>();
    private ArrayList<LatLng> markerPoints = new ArrayList<>();
    private RealmResults<Order> orders;

    public MapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.


    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View rootView = inflater.inflate(R.layout.activity_map, container, false);

        // To handle FAB animation upon entrance and exit
        final Animation growAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.simple_grow);
        final Animation shrinkAnimation = AnimationUtils.loadAnimation(getActivity(), R.anim.simple_shrink);


        fab = (FloatingActionButton) rootView.findViewById(R.id.gmail_fab);
        fab.setVisibility(View.VISIBLE);
        fab.startAnimation(growAnimation);


        coordinatorLayout = (CoordinatorLayout) rootView.findViewById(R.id.gmail_coordinator);
        bottomSheet = coordinatorLayout.findViewById(R.id.gmail_bottom_sheet);


        mapView = (MapView) rootView.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        MapsInitializer.initialize(getActivity());


        /**
         * Bottom Sheet
         */
        shrinkAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fab.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        behavior = BottomSheetBehavior.from(bottomSheet);
        behavior.setHideable(true);
        behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
        behavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                boolean showFAB = false;
                switch (newState) {

                    case BottomSheetBehavior.STATE_DRAGGING:

                        if (showFAB)
                            fab.startAnimation(shrinkAnimation);
                        break;

                    case BottomSheetBehavior.STATE_COLLAPSED:
                        showFAB = true;
                        fab.setVisibility(View.VISIBLE);
                        fab.startAnimation(growAnimation);
                        break;

                    case BottomSheetBehavior.STATE_EXPANDED:
                        showFAB = false;
                        break;


                }

            }

            @Override
            public void onSlide(View bottomSheet, float slideOffset) {

            }
        });


        RealmConfiguration rlmC = ((MainActivity) getActivity()).getRlmConfig();

        orders = OrdersManager.getInstance(rlmC).getOrders();


        orders.addChangeListener(new RealmChangeListener<RealmResults<Order>>() {
            @Override
            public void onChange(RealmResults<Order> results) {
                updateUI(results);
            }
        });

        return rootView;
    }

    private void updateUI(RealmResults<Order> results) {
        if (!results.isEmpty()) {

            Route route = RouteManager.getInstance(mCurrLocationMarker.getPosition()).calculateRoute(results);
            drawRoute(route);
            addMarkers(results, route.getPickUpLocation());
            addPickUpMarker(route.getPickUpLocation());

        } else {
            mMap.moveCamera(CameraUpdateFactory.newLatLng(mCurrLocationMarker.getPosition()));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(16));
        }


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        LocationRequest mLocationRequest = new LocationRequest();

        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setSmallestDisplacement(30);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY);
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    public void drawRoute(Route rt) {
        clearPolylines();
        clearOrderMarkers();

        PolylineOptions lineOptions = new PolylineOptions();

        if (!rt.getWaypoints().isEmpty()) {
            lineOptions.add(mCurrLocationMarker.getPosition());
            lineOptions.addAll(rt.getWaypoints());
            lineOptions.add(rt.getPickUpLocation().getLocation());
        }


        lineOptions.width(8);
        lineOptions.color(Color.BLUE);

        polylines.add(mMap.addPolyline(lineOptions));


    }

    private void clearPolylines() {
        for (Polyline po :
                polylines) {
            po.remove();
        }
        polylines.clear();
    }

    private void clearOrderMarkers() {
        for (Marker marker :
                orderMarkers) {
            marker.remove();
        }
        orderMarkers.clear();
    }

    private void addOrderMarker(Order order) {
        MarkerOptions options = new MarkerOptions();

        options.position(order.getLocation());
        if (markerPoints.size() == 1) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN));
        } else if (markerPoints.size() == 2) {
            options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED));
        }
        options.title(order.getRecipient().getPhone());
        options.snippet(order.getQuantity() + " Pizzas.");

        Marker markerToAdd = mMap.addMarker(options);
        orderMarkers.add(markerToAdd);

    }

    private void addPickUpMarker(PickUpLocation pul) {
        MarkerOptions options = new MarkerOptions();

        options.position(pul.getLocation());
        options.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE));

        options.title(pul.getName());
        options.snippet("Pick up Location");

        Marker markerToAdd = mMap.addMarker(options);
        orderMarkers.add(markerToAdd);

    }

    public void addMarkers(RealmResults<Order> orderList, PickUpLocation pickUpLocation) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(mCurrLocationMarker.getPosition());

        for (Order order : orderList) {
            addOrderMarker(order);
            builder.include(order.getLocation());
        }

        builder.include(pickUpLocation.getLocation());


        LatLngBounds bounds = builder.build();
        int padding = 100; // offset from edges of the map in pixels
        CameraUpdate cu;

        /*
        if(orderList.size()==1) {
            cu = CameraUpdateFactory.newLatLngZoom(orderList.get(0).getLocation(), 12F);
        }else {
            */
        cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        //}

        mMap.animateCamera(cu);

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        System.out.println("Location changed");

        updateLocation(location);

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Your Location");
        markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        updateUI(orders);

    }

    private void updateLocation(Location location) {

        /*todo
        try {
            MqttMessage mqttMessage = new MqttMessage(
                    MessagesManager
                            .locationUpdate(new LatLng(location.getLatitude(),location.getLongitude()),clientId)
                            .toString()
                            .getBytes()
            );
            client.publish(Topics.LOCATIONS,mqttMessage);
        } catch (MqttException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        */

    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }

        // Setting onclick event listener for the map
        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {

            @Override
            public void onMapClick(LatLng point) {
                Log.i("CLICKED LOCATION", "[Lat:" + point.latitude + " Long:" + point.longitude + "]");
                behavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //behavior.setPeekHeight(80);
                behavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
                return false;
            }
        });


    }
}