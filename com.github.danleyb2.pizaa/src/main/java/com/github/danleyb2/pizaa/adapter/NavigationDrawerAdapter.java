package com.github.danleyb2.pizaa.adapter;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.danleyb2.pizaa.R;

import java.util.Collections;
import java.util.List;

public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder> {
    List<Object> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;

    public NavigationDrawerAdapter(Context context, List<Object> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    private void setConnected(ImageView image) {
        Drawable doneCloud = ContextCompat.getDrawable(context, R.drawable.ic_cloud_done_dark);
        Drawable offCloud = ContextCompat.getDrawable(context, R.drawable.ic_cloud_off_dark);
        image.setImageDrawable(doneCloud);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        //MyViewHolder holder = new MyViewHolder(view);

        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        /*
        NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());
        */
        Drawable doneCloud = ContextCompat.getDrawable(context, R.drawable.ic_cloud_done_dark);
        Drawable offCloud = ContextCompat.getDrawable(context, R.drawable.ic_cloud_off_dark);
        holder.icon.setImageDrawable(doneCloud);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView icon;

        public MyViewHolder(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.title);
            //icon = (ImageView) itemView.findViewById(R.id.connection_icon);
        }
    }
}