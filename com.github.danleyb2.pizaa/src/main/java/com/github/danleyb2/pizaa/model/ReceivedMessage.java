package com.github.danleyb2.pizaa.model;

import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.util.Date;


public class ReceivedMessage {
    String topic;
    MqttMessage message;
    Date timestamp;

    public ReceivedMessage(String topic, MqttMessage message) {
        this.topic = topic;
        this.message = message;
        this.timestamp = new Date();
    }

    public String getTopic() {
        return topic;
    }

    public MqttMessage getMessage() {
        return message;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    @Override
    public String toString() {
        return "ReceivedMessage{" +
                "topic='" + topic + '\'' +
                ", message=" + message +
                ", timestamp=" + timestamp +
                '}';
    }
}
