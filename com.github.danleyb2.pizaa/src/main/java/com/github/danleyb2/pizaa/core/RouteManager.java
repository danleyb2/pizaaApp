package com.github.danleyb2.pizaa.core;

import com.github.danleyb2.pizaa.model.Order;
import com.github.danleyb2.pizaa.model.PickUpLocation;
import com.github.danleyb2.pizaa.model.Route;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import io.realm.RealmResults;


/**
 * Created by ndiek on 8/31/2016.
 */
public class RouteManager {

    private static RouteManager instance;
    //private RealmConfiguration rlmConfig;
    private static List<PickUpLocation> pickUpLocations = new ArrayList<>();

    static {
        pickUpLocations.add(new PickUpLocation(new LatLng(-1.290618, 36.824353), "Times tower"));
        pickUpLocations.add(new PickUpLocation(new LatLng(-1.293794, 36.854083), "Jogoo road"));
        pickUpLocations.add(new PickUpLocation(new LatLng(-1.278525, 36.821730), "Globe FlyOver"));
        pickUpLocations.add(new PickUpLocation(new LatLng(-1.254203, 36.798732), "Westlands"));
        pickUpLocations.add(new PickUpLocation(new LatLng(-1.275891, 36.779829), "Lavington"));
    }

    private LatLng currentLocation;

    public synchronized static RouteManager getInstance(LatLng position) {
        if (instance == null) {
            instance = new RouteManager();
            instance.setCurrentLocation(position);
        }
        return instance;
    }

    public static List<PickUpLocation> getPickUpLocations() {
        return pickUpLocations;
    }

    public static double calcltHyp(LatLng a, LatLng b) {
        //a2 +b2 = c2

        double width = (b.latitude - a.latitude);
        double height = (b.longitude - a.longitude);

        return Math.hypot(width, height);

    }

    public static PickUpLocation closestPickUp(final LatLng location) {

        Collections.sort(pickUpLocations, new Comparator<PickUpLocation>() {
            @Override
            public int compare(PickUpLocation lhs, PickUpLocation rhs) {
                double distanceLhs = calcltHyp(location, lhs.getLocation());
                double distanceRhs = calcltHyp(location, rhs.getLocation());
                return (distanceLhs > distanceRhs) ? 1 : -1;
            }
        });

        return pickUpLocations.get(0);

    }

    public static List<LatLng> orderWaypoints(final LatLng start, List<Order> remaining, List<LatLng> ordered) {
        if (remaining.isEmpty()) {
            return ordered;
        }
        Collections.sort(remaining, new Comparator<Order>() {
            @Override
            public int compare(Order lhs, Order rhs) {

                double distanceLhs = calcltHyp(start, lhs.getLocation());
                double distanceRhs = calcltHyp(start, rhs.getLocation());

                return (distanceLhs > distanceRhs) ? 1 : -1;

            }
        });

        Order nextStart = remaining.get(0);
        ordered.add(nextStart.getLocation());


        return orderWaypoints(nextStart.getLocation(), remaining.subList(1, remaining.size()), ordered);

    }

    public static void main(String... args) {


    }

    public LatLng getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(LatLng currentLocation) {
        this.currentLocation = currentLocation;
    }

    public Route calculateRoute(RealmResults<Order> orders) {

        List<Order> orderList = new ArrayList<>(orders);

        List<LatLng> ordered = new LinkedList<>();
        LatLng current = new LatLng(currentLocation.latitude, currentLocation.longitude);


        Route route = new Route();
        route.setWaypoints(orderWaypoints(current, orderList, ordered));
        route.setPickUpLocation(closestPickUp(ordered.get(ordered.size() - 1)));
        route.optimize();

        return route;

    }
}
