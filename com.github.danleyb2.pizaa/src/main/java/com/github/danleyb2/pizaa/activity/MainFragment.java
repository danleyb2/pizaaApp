package com.github.danleyb2.pizaa.activity;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.github.danleyb2.pizaa.R;

/**
 * Created by ndiek on 9/4/2016.
 */
public class MainFragment extends Fragment {


    private Switch connectSwitch;
    private TextView textViewClientHandle;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main, container, false);

        connectSwitch = (Switch) rootView.findViewById(R.id.switch_connect);
        textViewClientHandle = (TextView) rootView.findViewById(R.id.text_view_client_handle);


        connectSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {

                    ((MainActivity) getActivity()).connect();
                    changeConnectedState(true);

                } else {
                    ((MainActivity) getActivity()).disconnect();
                    changeConnectedState(false);
                }
            }
        });


        changeConnectedState(((MainActivity) getActivity()).isConnected());
        return rootView;
    }

    private void changeConnectedState(boolean b) {
        connectSwitch.setChecked(b);
        if (b) {
            textViewClientHandle.setText(((MainActivity) getActivity()).getClientId());
        } else {
            textViewClientHandle.setText("Not  Connected");
        }
    }

}
