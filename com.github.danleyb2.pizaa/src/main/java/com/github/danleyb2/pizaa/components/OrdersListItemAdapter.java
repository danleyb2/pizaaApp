package com.github.danleyb2.pizaa.components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.github.danleyb2.pizaa.R;
import com.github.danleyb2.pizaa.model.Order;

import java.util.List;


public class OrdersListItemAdapter extends ArrayAdapter<Order> {

    private final Context context;
    private final List<Order> orders;


    TextView messageTextView;
    TextView topicTextView;
    TextView dateTextView;

    public OrdersListItemAdapter(Context context, List<Order> orders) {
        super(context, R.layout.order_list_item, orders);
        this.context = context;
        this.orders = orders;

    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.order_list_item, parent, false);


            topicTextView = (TextView) convertView.findViewById(R.id.message_topic_text);
            messageTextView = (TextView) convertView.findViewById(R.id.message_text);
            //dateTextView = (TextView) rowView.findViewById(R.id.message_date_text);

            Order order = orders.get(position);

            messageTextView.setText(order.getRecipient().getName());
            topicTextView.setText("Quantity: " + String.valueOf(order.getQuantity()));

        }


        return convertView;
    }
}
