package com.github.danleyb2.pizaa.activity;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.github.danleyb2.pizaa.R;
import com.github.danleyb2.pizaa.config.Topics;
import com.github.danleyb2.pizaa.core.MessagesManager;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.json.JSONException;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import io.realm.RealmConfiguration;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    //private FragmentDrawer drawerFragment;
    private Toolbar mToolbar;
    private ChangeListener changeListener = new ChangeListener();


    private MainActivity mainActivity = this;

    //private ArrayList<String> connectionMap;
    private NavigationView nvDrawer;
    private DrawerLayout mDrawerLayout;


    private MqttAndroidClient statusUpdateIntentReceiver;


    private RealmConfiguration realmConfiguration;
    private String clientId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);

        setSupportActionBar(mToolbar);
        ActionBarDrawerToggle mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, mToolbar, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getActionBar().setTitle(mTitle);

            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                //getActionBar().setTitle(mDrawerTitle);

            }
        };

        mDrawerToggle.syncState();
        realmConfiguration = new RealmConfiguration.Builder(this)
                .deleteRealmIfMigrationNeeded()
                .build();


        // Setup drawer view
        setupDrawerContent(nvDrawer);
        mDrawerLayout.addDrawerListener(mDrawerToggle);

        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        PreferenceManager.setDefaultValues(this, R.xml.preferences, false);
        if (findViewById(R.id.container_body) != null) {
            // if we are being restored from a previous state, then we dont need to do anything and should
            // return or else we could end up with overlapping fragments.
            if (savedInstanceState == null) {
                getFragmentManager()
                        .beginTransaction()
                        .add(R.id.container_body, new MainFragment())
                        .commit();
                mDrawerLayout.openDrawer(GravityCompat.START);
            }
        }


    }

    public RealmConfiguration getRlmConfig() {
        return realmConfiguration;
    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    public MenuItem lastMenu = null;

                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        if (lastMenu != menuItem) {
                            lastMenu = menuItem;
                            switch (menuItem.getItemId()) {
                                case R.id.view_main:
                                    displayMainView();
                                    break;
                                case R.id.view_map:
                                    displayMapView();
                                    break;
                                case R.id.view_orders:
                                    Fragment ordersFragment = new OrdersFragment();
                                    displayFragment(ordersFragment, getString(R.string.orders_view));
                                    break;
                                case R.id.view_settings:
                                    displaySettingsView();
                                    break;
                                default:
                                    //fragmentClass = FirstFragment.class;
                            }
                        }
                        mDrawerLayout.closeDrawer(GravityCompat.START);
                        return true;
                    }
                });
    }

    private void displayMapView() {
        Fragment mapFragment = new MapFragment();
        displayFragment(mapFragment, getString(R.string.map_view));
    }

    private void displayMainView() {
        Fragment mapFragment = new MainFragment();
        displayFragment(mapFragment, getString(R.string.app_name));
    }

    public Context getThemedContext() {
        return getSupportActionBar().getThemedContext();
    }


    public void displaySettingsView() {
        Fragment editConnectionFragment = new SettingsFragment();
        String title = "Settings";
        displayFragment(editConnectionFragment, title);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    private void displayFragment(Fragment fragment, String title) {
        if (fragment != null) {

            //replaceFragment(fragment);


            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();


            // Set Toolbar Title
            getSupportActionBar().setTitle(title);
        }
    }

    public void setActionBarTitle(String title) {
        getSupportActionBar().setTitle(title);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
    }

    private void setUp() {
        MqttConnectOptions conOpt = new MqttConnectOptions();
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);

        // The basic client information
        String server = preferences.getString("host", "broker.hivemq.com");
        int port = Integer.parseInt(preferences.getString("port", "1883"));
        clientId = preferences.getString("rider_id", "danleyb2");
        boolean cleanSession = preferences.getBoolean("clean_session", true);


        String uri = "tcp://" + server + ":" + port;


        // last will message
        String last_will_message;
        final Integer qos = 0;
        Boolean retained = false;


        conOpt.setCleanSession(cleanSession);
        //conOpt.setConnectionTimeout(timeout);
        //conOpt.setKeepAliveInterval(keepalive);


        //if ((!message.equals(ActivityConstants.empty))|| (!topic.equals(ActivityConstants.empty))) {
        // need to make a message since last will is set]
        try {
            last_will_message = MessagesManager.lastWill(clientId).toString();
            conOpt.setWill(Topics.WILLS, last_will_message.getBytes(), qos, false);
        } catch (JSONException e) {
            Log.e(this.getClass().getCanonicalName(), "Exception Occured", e);
            e.printStackTrace();
            return;
        }


        statusUpdateIntentReceiver = new MqttAndroidClient(this, uri, clientId);
        statusUpdateIntentReceiver.setCallback(new MqttCallbackHandler(this, clientId, realmConfiguration));

        //set traceCallback
        statusUpdateIntentReceiver.setTraceCallback(new MqttTraceCallback());


        /*
        IntentFilter intentSFilter = new IntentFilter(MqttServiceConstants.CALLBACK_TO_ACTIVITY);
        LocalBroadcastManager.getInstance(this).registerReceiver(statusUpdateIntentReceiver, intentSFilter);

        */

        try {
            statusUpdateIntentReceiver.connect(conOpt, this, new IMqttActionListener() {
                @Override
                public void onSuccess(IMqttToken asyncActionToken) {
                    // We are connected
                    Log.d("MQTT CONNECTION", "onSuccess");


                    try {
                        statusUpdateIntentReceiver.subscribe("riders/" + clientId, 0);
                        String first_will_message = MessagesManager.firstWill(clientId).toString();
                        statusUpdateIntentReceiver.publish(
                                Topics.WILLS, first_will_message.getBytes(),
                                qos, false
                        );
                    } catch (MqttException | JSONException e) {
                        e.printStackTrace();
                    }


                }

                @Override
                public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                    // Something went wrong e.g. connection timeout or firewall problems
                    Log.d("MQTT CONNECTION", "onFailure");

                }
            });

        } catch (MqttException e) {
            Log.e(this.getClass().getCanonicalName(), "MqttException Occured", e);

        }
    }

    public void connect() {
        if (!isConnected())
            setUp();
    }


    public void disconnect() {
        try {
            statusUpdateIntentReceiver.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(statusUpdateIntentReceiver);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(statusUpdateIntentReceiver);


    }

    @Override
    protected void onPause() {
        //LocalBroadcastManager.getInstance(this).unregisterReceiver(statusUpdateIntentReceiver);
        super.onPause();

    }

    @Override
    protected void onResume() {
        super.onResume();
        //if (statusUpdateIntentReceiver != null) LocalBroadcastManager.getInstance(this).registerReceiver(statusUpdateIntentReceiver, new IntentFilter(MqttServiceConstants.CALLBACK_TO_ACTIVITY));
    }

    public boolean isConnected() {
        return statusUpdateIntentReceiver != null && statusUpdateIntentReceiver.isConnected();
    }

    public String getClientId() {
        return clientId;
    }

    private void replaceFragment(Fragment fragment) {
        String backStateName = fragment.getClass().getName();
        String fragmentTag = backStateName;

        FragmentManager manager = getFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped && manager.findFragmentByTag(fragmentTag) == null) {
            //fragment not in back stack, create it.
            FragmentTransaction ft = manager.beginTransaction();
            ft.replace(R.id.container_body, fragment, fragmentTag);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(backStateName);
            ft.commit();
        }
    }

    /**
     * This class ensures that the user interface is updated as the Connection objects change their states
     */
    private class ChangeListener implements PropertyChangeListener {

        /**
         * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
         */
        @Override
        public void propertyChange(PropertyChangeEvent event) {

            if (!event.getPropertyName().equals(ActivityConstants.ConnectionStatusProperty)) {
                return;
            }
            mainActivity.runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    //mainActivity.drawerFragment.notifyDataSetChanged();
                }

            });

        }

    }


}
