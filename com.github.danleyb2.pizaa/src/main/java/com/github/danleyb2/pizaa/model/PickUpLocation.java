package com.github.danleyb2.pizaa.model;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by ndiek on 9/2/2016.
 */
public class PickUpLocation {

    private LatLng location;
    private String name;

    public PickUpLocation(LatLng latLng, String name) {
        this.location = latLng;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public LatLng getLocation() {
        return location;
    }
}
