package com.github.danleyb2.pizaa.model;

import com.github.danleyb2.pizaa.core.RouteManager;
import com.google.android.gms.maps.model.LatLng;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by ndiek on 9/4/2016.
 */
public class Route {

    private PickUpLocation pickUpLocation;
    private List<LatLng> waypoints;

    public List<LatLng> getWaypoints() {
        return waypoints;
    }

    public void setWaypoints(List<LatLng> waypoints) {
        this.waypoints = waypoints;
    }

    public PickUpLocation getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(PickUpLocation pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public void optimize() {

        if (waypoints == null || pickUpLocation == null) {
            return;
        }

        Collections.sort(waypoints, new Comparator<LatLng>() {
            @Override
            public int compare(LatLng lhs, LatLng rhs) {
                double a = RouteManager.calcltHyp(getPickUpLocation().getLocation(), lhs);
                double b = RouteManager.calcltHyp(getPickUpLocation().getLocation(), rhs);
                return a > b ? -1 : 1;
            }
        });

    }
}
