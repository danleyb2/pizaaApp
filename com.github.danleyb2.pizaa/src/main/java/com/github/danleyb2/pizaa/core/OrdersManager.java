package com.github.danleyb2.pizaa.core;

import com.github.danleyb2.pizaa.model.Order;

import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;


/**
 * Created by ndiek on 8/19/2016.
 */
public class OrdersManager {

    private static OrdersManager instance;
    private Realm realm;

    private OrdersManager() {

    }

    private OrdersManager(RealmConfiguration rlmConfig) {
        realm = Realm.getInstance(rlmConfig);
    }

    public static synchronized OrdersManager getInstance(RealmConfiguration rlmConfig) {
        if (instance == null) {
            instance = new OrdersManager(rlmConfig);
        }
        return instance;
    }

    public RealmResults<Order> getOrders() {
        return realm.where(Order.class).findAll();
    }

    public void addOrder(Order newOrder) {
        //todo ordersList.add(newOrder);
    }
}
