package com.github.danleyb2.pizaa.activity;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

import com.github.danleyb2.pizaa.R;
import com.github.danleyb2.pizaa.components.OrdersListItemAdapter;
import com.github.danleyb2.pizaa.model.Order;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;


public class OrdersFragment extends Fragment {

    ListView ordersListView;
    OrdersListItemAdapter ordersListAdapter;
    Button clearButton;
    private Realm realm;

    private RealmResults<Order> orders;

    public OrdersFragment() {
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //Map<String, Connection> connections = Connections.getInstance(this.getActivity()).getConnections();


        //connection = connections.get(this.getArguments().getString(ActivityConstants.CONNECTION_KEY));


        //messageListAdapter.notifyDataSetChanged();
        /*
        read orders from realm

        messages = connection.getMessages();
        connection.addReceivedMessageListner(new IReceivedMessageListener() {
            @Override
            public void onMessageReceived(ReceivedMessage message) {
                System.out.println("GOT A MESSAGE in history " + new String(message.getMessage().getPayload()));
                System.out.println("M: " + messages.size());
                messageListAdapter.notifyDataSetChanged();
            }
        });
        */

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View rootView = inflater.inflate(R.layout.fragment_orders, container, false);


        realm = Realm.getInstance(((MainActivity) getActivity()).getRlmConfig());
        orders = realm.where(Order.class).findAll();

        ordersListAdapter = new OrdersListItemAdapter(getActivity(), orders);

        ordersListView = (ListView) rootView.findViewById(R.id.history_list_view);
        ordersListView.setAdapter(ordersListAdapter);

        clearButton = (Button) rootView.findViewById(R.id.history_clear_button);
        clearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                realm.beginTransaction();
                orders.deleteAllFromRealm();
                realm.commitTransaction();
                ordersListAdapter.notifyDataSetChanged();
            }
        });

        orders.addChangeListener(new RealmChangeListener<RealmResults<Order>>() {
            @Override
            public void onChange(RealmResults<Order> results) {
                ordersListAdapter.notifyDataSetChanged();
            }
        });

        // Inflate the layout for this fragment
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }


}

