package com.github.danleyb2.pizaa.components;


public interface ITextSelectCallback {
    void onTextUpdate(String updatedText);
}
