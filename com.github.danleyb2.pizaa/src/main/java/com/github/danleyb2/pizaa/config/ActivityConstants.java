/*******************************************************************************
 * Copyright (c) 1999, 2014 IBM Corp.
 * <p/>
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * <p/>
 * The Eclipse Public License is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 */
package com.github.danleyb2.pizaa.config;

import org.eclipse.paho.client.mqttv3.MqttMessage;


/**
 * This Class provides constants used for returning results from an activity
 */
public class ActivityConstants {

    /**
     * Default QOS value
     */
    public static final int defaultQos = 0;

  /*Default values **/
    /**
     * Default timeout
     */
    public static final int defaultTimeOut = 1000;
    /**
     * Default keep alive value
     */
    public static final int defaultKeepAlive = 10;
    /**
     * Show History Request Code
     **/
    public static final int showHistory = 3;
    /**
     * Server Bundle Key
     **/
    public static final String server = "broker.hivemq.com";
    /**
     * Port Bundle Key
     **/
    public static final String port = "1883";
    /**
     * Message Bundle Key
     **/
    public static final String message = "bye!!!";
    /**
     * Retained Flag Bundle Key
     **/
    public static final boolean retained = false;
    /**
     * QOS Value Bundle Key
     **/
    public static final int qos = 0;
    /**
     * SSL Enabled Flag Bundle Key
     **/
    public static final boolean ssl = false;
    /**
     * Clean Session Flag Bundle Key
     **/
    public static final boolean cleanSession = false;
    /**
     * Property name for the connection status field in {@link Connection} object for use with {@link java.beans.PropertyChangeEvent}
     **/
    public static final String ConnectionStatusProperty = "connectionStatus";

  /* Bundle Keys */
    /**
     * Empty String for comparisons
     **/
    public static final String empty = new String();
    /**
     * Application TAG for logs where class name is not used
     */
    static final String TAG = "MQTT Android";
    /** ClientID Bundle Key **/
    //public static final String clientId = "danleyb2";//+(int)(Math.random() * 504 + 1);
    /**
     * Default SSL enabled flag
     */
    static final boolean defaultSsl = false;
    /**
     * Default message retained flag
     */
    static final boolean defaultRetained = false;
    /**
     * Default last will message
     */
    static final MqttMessage defaultLastWill = null;
    /**
     * Default port
     */
    static final int defaultPort = 1883;
    /**
     * Connect Request Code
     */
    static final int connect = 0;
    /**
     * Advanced Connect Request Code
     **/
    static final int advancedConnect = 1;
    /**
     * Last will Request Code
     **/
    static final int lastWill = 2;
    /**
     * History Bundle Key
     **/
    static final String history = "history";
    /**
     * User name Bundle Key
     **/
    static final String username = "username";
    /**
     * Password Bundle Key
     **/
    static final String password = "password";
    /**
     * Keep Alive value Bundle Key
     **/
    static final String keepalive = "keepalive";
    /**
     * Timeout Bundle Key
     **/
    static final String timeout = "timeout";
    /**
     * SSL Key File Bundle Key
     **/
    static final String ssl_key = "ssl_key";

  /* Property names */
    /**
     * Connections Bundle Key
     **/
    static final String connections = "connections";
    /**
     * Action Bundle Key
     **/
    static final String action = "action";

  /* Useful constants*/
    /**
     * Property name for the history field in {@link Connection} object for use with {@link java.beans.PropertyChangeEvent}
     **/
    static final String historyProperty = "history";
    /**
     * Space String Literal
     **/
    static final String space = " ";
    public static String clientHandle;


}
