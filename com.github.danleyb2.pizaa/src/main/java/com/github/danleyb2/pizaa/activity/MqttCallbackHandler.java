/*******************************************************************************
 * Copyright (c) 1999, 2014 IBM Corp.
 * <p/>
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * and Eclipse Distribution License v1.0 which accompany this distribution.
 * <p/>
 * The Eclipse Public License is available at
 * http://www.eclipse.org/legal/epl-v10.html
 * and the Eclipse Distribution License is available at
 * http://www.eclipse.org/org/documents/edl-v10.php.
 */
package com.github.danleyb2.pizaa.activity;

import android.content.Context;
import android.util.Log;

import com.github.danleyb2.pizaa.config.Keys;
import com.github.danleyb2.pizaa.model.Order;
import com.github.danleyb2.pizaa.model.Person;
import com.google.android.gms.maps.model.LatLng;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttCallbackExtended;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.json.JSONException;
import org.json.JSONObject;

import io.realm.Realm;
import io.realm.RealmConfiguration;

//import org.eclipse.paho.android.sample.Connection.ConnectionStatus;

/**
 * Handles call backs from the MQTT Client
 */
public class MqttCallbackHandler implements MqttCallback, MqttCallbackExtended {

    private static final String TAG = "MqttCallbackHandler";
    private static final String activityClass = "com.github.danleyb2.pizaa.activity.MainActivity";
    private final RealmConfiguration rlmConfig;
    /**
     * {@link Context} for the application used to format and import external strings
     **/
    private Context context;
    /**
     * Client handle to reference the connection that this handler is attached to
     **/
    private String clientHandle;

    public MqttCallbackHandler(Context context, String clientHandle, RealmConfiguration rlmConfig) {
        this.context = context;
        this.clientHandle = clientHandle;
        this.rlmConfig = rlmConfig;
    }

    /**
     * @see org.eclipse.paho.client.mqttv3.MqttCallback#connectionLost(java.lang.Throwable)
     */
    @Override
    public void connectionLost(Throwable cause) {
        if (cause != null) {
            Log.d(TAG, "Connection Lost: " + cause.getMessage());

        }
    }

    /**
     * @see org.eclipse.paho.client.mqttv3.MqttCallback#messageArrived(java.lang.String, org.eclipse.paho.client.mqttv3.MqttMessage)
     */
    @Override
    public void messageArrived(String topic, MqttMessage message) throws Exception {
        Log.i("Message", new String(message.getPayload()));

        switch (topic) {
            default:
                if (topic.equals("riders/" + clientHandle)) {
                    try {

                        final JSONObject order = new JSONObject(new String(message.getPayload()));

                        JSONObject location = order.getJSONObject(Keys.LOCATION);
                        final JSONObject receiver = order.getJSONObject(Keys.RECEIVER);
                        final LatLng endpoint = new LatLng(
                                location.getDouble(Keys.LATITUDE),
                                location.getDouble(Keys.LONGITUDE)
                        );

                        Realm.getInstance(rlmConfig).executeTransaction(new Realm.Transaction() {

                            @Override
                            public void execute(Realm realm) {
                                try {

                                    int id = order.getInt("index");
                                    int quantity = order.getInt("quantity");

                                    Order order = realm.where(Order.class).equalTo("id", id).findFirst();
                                    if (order == null) {
                                        order = realm.createObject(Order.class, id);

                                        String pId = receiver.getString("phone");
                                        Person person = realm.where(Person.class).equalTo("phone", pId).findFirst();
                                        if (person == null) {
                                            person = realm.createObject(Person.class, pId);
                                            person.setName(receiver.getString("name"));
                                        }

                                        order.setQuantity(quantity);
                                        order.setRecipient(person);
                                    }

                                    order.setLocation(endpoint);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            }

                        });


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
        }


    }

    /**
     * @see org.eclipse.paho.client.mqttv3.MqttCallback#deliveryComplete(org.eclipse.paho.client.mqttv3.IMqttDeliveryToken)
     */
    @Override
    public void deliveryComplete(IMqttDeliveryToken token) {
        // Do nothing
    }

    @Override
    public void connectComplete(boolean reconnect, String serverURI) {

    }
}
