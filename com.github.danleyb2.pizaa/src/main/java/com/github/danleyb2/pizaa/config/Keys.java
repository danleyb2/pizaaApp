package com.github.danleyb2.pizaa.config;

/**
 * Created by ndiek on 8/18/2016.
 */
public class Keys {
    //public static String CLIENT = "client";
    public static String RIDER = "rider";
    public static String LOCATION = "location";
    public static String LATITUDE = "latitude";
    public static String LONGITUDE = "longitude";
    public static String NAME = "name";
    public static String DIRECTION = "route";
    public static String ALIVE = "alive";
    public static String ORDERS = "orders";
    public static String RECEIVER = "recipient";
}
