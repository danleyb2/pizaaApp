# Pizaa Android App

The Server-broker is used to pick the closest rider to the order,  
The new order is then sent to the chosen rider's app which reorders his current orders (if he/she has any) to get the shortest route through all of them starting from his/her current location.

## Set up

#### To test Just the App

A new order should be sent to the app in this **json** structure
```
	{
	    "_id": "",
	    "index": "457", //order id, given to the order by the broker-server
	    "recipient": { 	//receiver details
	      "phone": "+1 (862) 569-3575",
	      "name": "Kristin Edwards"
	    },
	    "quantity": 2, 	//number of boxes ordered from the location
	    "location": { 	//order delivery location
	      "address": "104 Eldert Lane, Salvo, New York", 
	      "latitude": -1.313878,
	      "longitude": 36.830553
	    }
	  }
```



Download an mqtt client, I recommend [MqttFX](http://mqttfx.jfx4ee.org/index.php/download)  
Connect to an mqtt broker of your choosing 

Start the app and enter the same broker configurations as the mqtt client above

Select the Main menu and toggle the connect switch.

Publish the following orders to the topic `orders/[Rider ID]` (e.g `riders/danleyb2`) one at a time, (or you could create your own set of orders)

Select the map menu to view the route redrawn each time you publish each new order.

```
  {
    "_id": "57cb3b23d4204554aa389f26",
    "index": 0,
    "recipient": {
      "phone": "+1 (862) 569-3575",
      "name": "Kristin Edwards"
    },
    "quantity": 2,
    "location": {
      "address": "104 Eldert Lane, Salvo, New York",
      "latitude": -1.313878,
      "longitude": 36.830553
    }
  }


  {
    "_id": "57cb3b23508c458462e7a6c0",
    "index": 1,
    "recipient": {
      "phone": "+1 (820) 552-2179",
      "name": "Vang Simon"
    },
    "quantity": 7,
    "location": {
      "address": "305 Lake Street, Sanborn, Illinois",
      "latitude": -1.299690,
      "longitude": 36.836842
    }
  }

  {
    "_id": "57cb3b23277772747ece4acd",
    "index": 2,
    "recipient": {
      "phone": "+1 (915) 419-3871",
      "name": "Ward Beard"
    },
    "quantity": 6,
    "location": {
      "address": "266 Knight Court, Deputy, Iowa",
      "latitude": -1.299746,
      "longitude": 36.824098
    }
  }

  {
    "_id": "57cb3b233468244c8c17b3e9",
    "index": 3,
    "recipient": {
      "phone": "+1 (950) 595-3722",
      "name": "Mann Huffman"
    },
    "quantity": 7,
    "location": {
      "address": "187 Grand Street, Tolu, Utah",
      "latitude": -1.295191,
      "longitude": 36.812737
    }
  }

  {
    "_id": "57cb3b23fba1191109f7f8a9",
    "index": 4,
    "recipient": {
      "phone": "+1 (852) 467-2744",
      "name": "Michelle Stone"
    },
    "quantity": 6,
    "location": {
      "address": "665 Albany Avenue, Richmond, Rhode Island",
      "latitude": -1.295191,
      "longitude": 36.912737
    }
  }

```


#### To test the App and the Server Broker
The server broker is used to pick the closest rider to the new order from the currently  online riders, it could also be used to check if the rider satisfys the delivery by having the appropriate number of boxes
You will need more than one device at different locations, you could use genymotion and configure the device location